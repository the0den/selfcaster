from distutils.core import setup

setup(
    name = 'selfcaster',
    version = '0.1',
    author = 'Denis Untevskiy',
    author_email = 'd.untevskiy@gmail.com',
    url = 'https://bitbucket.org/theoden/selfcaster',
    description = 'Personal communication platform - your personal command center to communicate in public (to blog-platforms or social networks) or in private (email, IMs or social networks).',
    long_description = open('README.rst').read(),
    packages = ['selfcaster'],
    license = 'BSD',
    classifiers = [
        'Programming Language :: Python',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Development Status :: 1 - Planning',
        'Environment :: Web Environment',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Communications',
        'Topic :: Communications :: Chat',
        'Topic :: Communications :: Conferencing',
        'Topic :: Communications :: Email',
        'Topic :: Internet',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: News/Diary',
    ]
)