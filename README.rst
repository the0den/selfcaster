============
 Selfcaster
============

Selfcaster is a personal communication platform. It allows an enduser
to communicate in public (to blog-platforms or social networks) or in private
(email, IMs or social networks). And, that's important, do it in one place.

Main goals are:

*  privacy
*  full ownnership on history with fast and flexible search
*  support of most-popular broadcast channels

Technically Selfcaster is a *project* for the `Django framework`_.

Important legal notification
----------------------------

BSD-license is the **preliminary** one. It may be reconsidered in the nearest
releases.

.. _Django framework: https://www.djangoproject.com
